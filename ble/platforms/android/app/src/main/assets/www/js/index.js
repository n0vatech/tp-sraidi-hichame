/* 
 * Fichier modèle pour l'édition d'un script
 */

//---------------------------------------------------------------------------
// Sources de données, constantes
const reset = '{"reset":"true"}'
const erase = '{"erase":true}'
//---------------------------------------------------------------------------
// Variables globales
var DeviceName = null;
var DeviceID = null;
var DeviceService = null;
var DeviceCharacteristic = null;
var Deviceproperties = null;
var SSID = null;
var PASSWORD = null;

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Programme principal avec jQuery
$(document).ready(main);           // attente chargement du DOM

function main() {                   // programme principal
    /**** Gestion Bouton Scan ****/
    $('#btnScan').on("click", onClickBtnScan);

    /**** Gestion Boutons Des differentes pages ****/
    $(":mobile-pagecontainer").on("pagecontainerchange", function (event, ui) {
        if (ui.toPage[0].id == "p_device") {
            $('#btnRead').on("click", onClickBtnRead);
            $('#btnSend').on("click", onClickBtnSend);
            $('#btnDisconnect').on("click", OnClickBtnDisconnect);
            $('#btnErase').on("click", onClickBtnErase);
            $('#btnReset').on("click", onClickBtnReset);
            $('#ssidField').on("click", function () { $('#ssidField').val('') });
            $('#passwordField').on("click", function () { $('#passwordField').val('') });
            $('#btnRetour').on("click", onClickBtnRetour);
        }
    });
}


//---------------------------------------------------------------------------
// Fonctions évènementielles

function onClickBtnScan(e) {
    /**** Verifications allumage du bluetooth ****/
    ble.isEnabled(
        function () {
            e.preventDefault();
            $('#listDevices').html('');
            $('#listDevices').append('<h1>Appareil trouvés</h1>');

            /**** Demarrer le scan des differents périphériques ****/
            scanDevices();

        },

        /****Avvertissement bluetooth si éteint  ****/
        function () {
            $('#popupScan').html('<p>Veuillez d\'abord activer le Bluetooth</p>');
        }
    );
}




//---------------------------------------------------------------------------
// Fonctions utilitaires

function scanDevices() {
    ble.startScan([], function (device) {
        /**** Filtrage des undefined ****/
        if (device.name !== undefined) {
            /**** Affichage des appareils trouvés ****/
            $('#listDevices').append('<li><a href="#" data-transition="none" data-index="' + device.id + '">' + device.name + '</a></li>');
            $("#listDevices").listview("refresh");

            /**** Gestionnaires des appareils trouvés ****/
            $('#p_accueil ul li a').on("click", OnClickOnDevice);
        }
    });

    /**** Definition du temps de scan ****/
    setTimeout(ble.stopScan,
        3000,
        function () { console.log("Scan complete"); },
        function () { console.log("stopScan failed"); }
    );

}

/**** Appui sur un périphérique ****/
function OnClickOnDevice(e) {

    /**** Desactiver le comportement normal du lien ****/
    e.preventDefault();

    /**** Recuperation nom de l'appareil ****/
    DeviceName = $(this).text();

    /**** Recuperation id de l'appareil ****/
    DeviceID = $(this).attr('data-index');

    /**** Chargement de la nouvelle page p_device****/
    $(':mobile-pagecontainer').pagecontainer(
        'change',
        '#p_device',
        { transition: 'none' }); //inhibe l'effet de surbrillance

    /**** Connexion à l'appareil ****/
    ble.connect(DeviceID, function (device) {
        setTimeout(function () {
            $('#chargement_section').html('<h1 id="chargement_section_titre">Connecté a l\'appareil ' + DeviceName + '</h1>');
            $('#afterLoad').show();
            /**** Notification de connexion audio ****/
            vocalNotification('Connecté a l\'appareil ' + DeviceName);
        }, 3500);

        /**** Recuperation des services BLE ****/
        device.characteristics.forEach(function (element) {
            if (element.properties[0] == 'Read' && element.properties[1] == 'Write') {
                DeviceService = element.service;
                DeviceCharacteristic = element.characteristic;
            }

        })
    });
}

/**** Gestion bouton Lire ****/
function onClickBtnRead(e) {
    /**** Lecture infos ESP ****/
    ble.read(DeviceID, DeviceService, DeviceCharacteristic,
        function (data) {

            /**** Créations d'une vue (tableau) pour lecture objet buffer ****/
            var uint8View = new Uint8Array(data);

            /**** Passage de ASCII à STRING ****/
            const uistring = String.fromCharCode.apply(null, uint8View);
            /**** Extraction key d'un objet json ****/
            let objCredentials = JSON.parse(uistring);
            /**** Messages de notification pour configuration ****/
            if (objCredentials.ssidPrim == '') {
                $('#popupRead').html(DeviceName + ' ne possede aucune configuration');
                $('#textarea-1').html('');
                $('#textarea-1').append('Aucune configuration est disponible, veuillez mettre a jour l\'esp');
            } else {
                $('#popupRead').html(DeviceName + ' est configuré pour se connecter à ' + objCredentials.ssidPrim);
                $('#textarea-1').html('');
                $('#textarea-1').append('Vous avez demander la configuration \nActuellement le SSID est : ' + objCredentials.ssidPrim + '\nActuellement le Password est : ' + objCredentials.pwPrim);
            }

        },
        function (failure) {
            console.log("Failed to read characteristic from device.");
        });
}

/**** Gestion bouton Écrire ****/
function onClickBtnSend() {
    /**** Recuperation SSID et password ****/
    SSID = $('#ssidField').val();
    PASSWORD = $('#passwordField').val();
    /**** Creation objet JSON ****/
    var credentials = '{"ssidPrim":"' + SSID + '","pwPrim":"' + PASSWORD + '"}';
    console.log(credentials);
    /**** Créations d'une vue (tableau) pour création objet buffer ****/
    var array = new Uint8Array(credentials.length);

    /**** Remplissage vue (tableau) avec passage de STRING à ASCII ****/
    for (var i = 0; i < credentials.length; i++) {
        array[i] = credentials.charCodeAt(i);
    }
    /**** Encapsulation vers objet buffer ****/
    var credentialsObjBuffer = array.buffer

    /**** Envoi des données ****/
    ble.write(DeviceID, DeviceService, DeviceCharacteristic, credentialsObjBuffer,
        function () {
            console.log('data envoyer');
            $('#textarea-1').html('');
            $('#textarea-1').append('Configuration envoyé a l\'esp');
        },
        function () {
            console.log('data lost');
        });
}

/**** Gestion bouton Éffacer ****/
function onClickBtnErase() {

    /**** Créations d'une vue (tableau) pour création objet buffer ****/
    var array = new Uint8Array(erase.length);

    /**** Remplissage vue (tableau) avec passage de STRING à ASCII ****/
    for (var i = 0; i < erase.length; i++) {
        array[i] = erase.charCodeAt(i);
    }
    /**** Encapsulation vers objet buffer ****/
    var eraseObjBuffer = array.buffer;

    /**** Envoi des données ****/
    ble.write(DeviceID, DeviceService, DeviceCharacteristic, eraseObjBuffer,
        function () {
            console.log('data envoyer');
            $('#popupRead').html(DeviceName + ' ne possede aucune configuration');
            $('#textarea-1').html('');
                $('#textarea-1').append('Configuration de l\'esp effacer');
        },
        function () {
            console.log('data lost');
        });

}

/**** Gestion bouton Réinitialiser ****/
function onClickBtnReset() {
    console.log(DeviceName);
    console.log(DeviceService);
    console.log(DeviceCharacteristic);
    console.log(DeviceID);
    /**** Créations d'une vue (tableau) pour création objet buffer ****/
    var array = new Uint8Array(reset.length);

    /**** Remplissage vue (tableau) avec passage de STRING à ASCII ****/
    for (var i = 0; i < reset.length; i++) {
        array[i] = reset.charCodeAt(i);
    }
    /**** Encapsulation vers objet buffer ****/
    var resetObjBuffer = array.buffer;

    /**** Envoi des données ****/
    ble.write(DeviceID, DeviceService, DeviceCharacteristic, resetObjBuffer,
        function () {
            console.log('data envoyer');
        },
        function () {
            console.log('data lost');
        });
}

/**** Gestion bouton Déconnexion ****/
function OnClickBtnDisconnect() {

    /**** Mise à jour affichage du DOM ****/
    $('#chargement_section').html('<h1 id="chargement_section_titre">Déconnecter de l\'appareil ' + DeviceName + '</h1>')
    ble.disconnect(DeviceID);
    console.log("deconnexion");
}


function onClickBtnRetour() {

    ble.disconnect(DeviceID, function () {
        $('#chargement_section').html('<h1 id="chargement_section_titre">Connection a l\'appareil</h1><img src="img/chargement2.gif">')
        $('#afterLoad').hide();
        /**** Chargement de la nouvelle page ****/
        $(':mobile-pagecontainer').pagecontainer(
            'change',
            '#p_accueil',
            { transition: 'none' }); //inhibe l'effet de surbrillance
        $('#listDevices').html('');
        /**** Notification de déconnexion audio ****/
        vocalNotification('Déconnecter de l\'appareil ' + DeviceName)

    }, function (error) {
        console.log(error);
    });
}
/**** Simulation clique ****/
function eventFire(el, etype) {
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}

//eventFire(document.getElementById('mytest1'), 'click');

/**** Fonction pour notification audio ****/
function vocalNotification(message) {

    TTS.speak({
        text: message, // message a lire
        locale: 'fr-FR', // langue utliser
        rate: 0.7 // vitesse de lecture
    }, function () {
        console.log('Text succesfully spoken');
    }, function (reason) {
        console.log(reason);
    });
}